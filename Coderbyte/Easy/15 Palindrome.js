function Palindrome(str) {
  var str = str.replace(/\s/g, '').toLowerCase();
  var rev = str.split('').reverse().join('');
  return str === rev;
}
   
// keep this function call here 
Palindrome(readline());
