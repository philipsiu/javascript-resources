Closure
Being able to reference a specific instance of local variables in an enclosing function
A function that “closes over” some local variables is called a closure

Recursion
A function that calls itself is called recursive
Always need a base case
Slower than loop
Use when processing several branches, that can branch out into more branches

Hoisting
All declarations, both functions and variables, are hoisted to the top of the containing scope, before any part of your code is executed.
var x = 2;
var names = function() {};
Functions are hoisted first, and then variables.
function age() {}
var x;
Function declarations have priority over variable declarations, but not over variable assignments.

Ternary Conditionals
var x = male ? 'boy' : 'girl';
console.log('hello' + male ? 'boy' : 'girl'); //bad
console.log('hello' + (male ? 'boy' : 'girl')); //good

Logical Operators
||  OR Operator
Will select the first value it encounters that is truthy
var x = 1 || 2; //1
When all elements are falsy, will select the last falsy value found
var x = undefined || ''; //''

&& AND Operator
When all elements are truthy, will return the last truthy value found
var x = 1 && 2; //2

Loop Optimization
//bad
var numbers = {1: 1, 2: 2, 3: 3, ten: [10, 10, 10]};
for (var i = 0; i < numbers.ten.length; i++) {
  console.log(array.ten[i]);
}
At the start of each potential loop cycle, the program will need to find and retrieve:
1. the value of i
2. the numbers object
3. the ten property
4. the array pointed to by the property
5. the length property of the array

//good
var numbers = {1: 1, 2: 2, 3: 3, ten: [10, 10, 10]};
var list = array.ten;
for (var i = 0, x = array.ten.length; i < x; i += 1) {
  console.log(list[i]);
}

Use a Prototype for Shared Stuff
function People(firstName, lastName) {
  this.firstName = firstName;
  this.lastName = lastName;
  this.greet = function () {
    console.log('Hello ' + this.firstName + this.lastName)
  }
} //bad

prototype should be used for shared properties and methods in order to increase memory efficiency
function People(firstName, lastName) {
  this.firstName = firstName;
  this.lastName = lastName;
}
People.prototype = {
  greet: function () {
    console.log('Hello ' + this.firstName + this.lastName)
  }
} //good

Use a Document Fragment to Insert Additions all at once
//bad
var list = document.getElementById('names');
var people = ['bob', 'sam', 'gary'];
for (var i = 0, x = people.length; i < x; i += 1) {
  var element = document.createElement('li');
  element.appendChild(document.createTextNode(people[i]));
  list.appendChild(element);
}

//good
var list = document.getElementById('names');
var people = ['bob', 'sam', 'gary'];
var fragment = document.createDocumentFragment();
var element;
for (var i = 0, x = people.length; i < x; i += 1) {
  element = document.createElement('li');
  element.appendChild(document.createTextNode(people[i]));
  fragment.appendChild(element);
}
list.appendChild(fragment);

Dont use With, use a variable to cache
var apple = tree.apple

Dont use eval, use data structures and JSON.parse()

Use toFixed for decimals
var num = .1 + .2;
console.log(num); //bad
console.log(num.toFixed(1)); //good

Use parseFloat() to turn strings with decimals into numbers
Use parseInt() to convert numerical strings, but rounds down. Always use a radix value between 2-36
parseInt('055', 10); //55

If unsure about data type, but highly reliant on a Number, use typeof and isNaN()
function numberCheck(data) {
  return typeof data === 'number' && !isNaN(data);
}

Use namespace, an object that groups and protects related data and methods in JavaScript files
Make sensitive data private by closure, and public properties are accessible through the namespace
var PEOPLE = (function() {
  var firstNames = ['bob', 'sam'];
  var removeFirstName = function () {};
  return {
    addFirstName: function () {};
  }
})();
